import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Scanner;

public class BookDAO {
	private static JDBCConnection jdbcConnection = new JDBCConnection();
	
	public Books getBookById(int bookId) {
		Books book = new Books();
		Connection connection = null;
		try {
			connection = jdbcConnection.getConnection();
			String sql = "select * from books where book_id = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, bookId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				book.setBookId(resultSet.getInt("book_id"));
				book.setCategoryId(resultSet.getInt("category_id"));
				book.setBookName(resultSet.getString("book_name"));
				book.setBookRate(resultSet.getDouble("book_rate"));
				book.setBookAuthor(resultSet.getString("book_author"));
				book.setBookPrice(resultSet.getDouble("book_price"));
				book.setBookQuantity(resultSet.getInt("book_quantity"));
				book.setBookStatus(resultSet.getBoolean("book_status"));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return book;
	}

	public Books getBookByName(String bookName) {
		Books book = new Books();
		Connection connection = null;
		try {
			connection = jdbcConnection.getConnection();
			String sql = "select * from books where book_name = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, bookName);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				book.setBookId(resultSet.getInt("book_id"));
				book.setCategoryId(resultSet.getInt("category_id"));
				book.setBookName(resultSet.getString("book_name"));
				book.setBookRate(resultSet.getDouble("book_rate"));
				book.setBookAuthor(resultSet.getString("book_author"));
				book.setBookPrice(resultSet.getDouble("book_price"));
				book.setBookQuantity(resultSet.getInt("book_quantity"));
				book.setBookStatus(resultSet.getBoolean("book_status"));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return book;
	}
	
	boolean updateBook(int book_id, int category_id, String book_name, double book_rate, String book_author, double book_price, int book_quantity) {
		boolean check = true;
		Connection connection = null;
		boolean book_status = book_quantity > 0;
		String sql = "update books set category_id = ?, book_name = ?, book_rate = ?, book_author = ?, book_quantity = ?, book_status = ? where book_id = ?";
		try {
			connection = jdbcConnection.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, category_id);
			preparedStatement.setString(2, book_name);
			preparedStatement.setDouble(3,book_rate );
			preparedStatement.setString(4, book_author);
			preparedStatement.setDouble(5, book_quantity);
			preparedStatement.setBoolean(6, book_status);
			preparedStatement.setInt(7, book_id);
			preparedStatement.execute();
			preparedStatement.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			check = false;
			// TODO: handle exception
		}
		
		return check;
	}
	
	boolean insertBook(int category_id, String book_name, double book_rate, String book_author, double book_price, int book_quantity, boolean book_status) {
		Books book = this.getBookByName(book_name);
		if(book.getBookName() != null) return false;
		Connection connection = null;
		String sql = "insert into books(category_id, book_name,  book_rate, book_author, book_price, book_quantity, book_status ) values(?, ?, ?, ?, ?, ?, ?)";
		boolean check = true;
		try {
			connection = jdbcConnection.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, category_id);
			preparedStatement.setString(2, book_name);
			preparedStatement.setDouble(3, book_rate);
			preparedStatement.setString(4, book_author);
			preparedStatement.setDouble(5, book_price);
			preparedStatement.setInt(6, book_quantity);
			preparedStatement.setBoolean(7, book_status);
			preparedStatement.execute();
			preparedStatement.close();
			connection.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			check = false;
		}
		
		return check;
	}
	
	boolean deleteBook(int book_id) {
		boolean check = true;
		Connection connection = null;
		String sql = "delete from books where book_id = ?";
		try {
			connection = jdbcConnection.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, book_id);
			preparedStatement.execute();
			preparedStatement.close();
		} catch (Exception e) {
			// TODO: handle exception
			check = false;
			e.printStackTrace();
		}
		
		return check;
	}
	
	
	public static void main(String[] args) {
		BookDAO bookDAO = new BookDAO();
		try {
		      File myObj = new File("book.txt");
		      Scanner myReader1 = new Scanner(myObj);
		      myReader1.hasNext();
		      while(myReader1.hasNext()) {
		    	  String data = myReader1.nextLine();
		    	  Scanner myReader = new Scanner(data);
		    	  int category_id = myReader.nextInt();
		    	  String book_name = myReader.next();
		    	  double book_rate= myReader.nextDouble(); 
		    	  String book_author = myReader.next();
		    	  double book_price= myReader.nextDouble();
		    	  int book_quantity= myReader.nextInt();
		    	  boolean book_status = myReader.hasNextBoolean();
		    	  
		    	  boolean check = bookDAO.insertBook(category_id, book_name, book_rate, book_author, book_price, book_quantity,book_status );
		    	  if(check == false) {
		    		  Books book = bookDAO.getBookByName(book_name);
		    		  boolean check2= bookDAO.updateBook(book.getBookId(), category_id, book_name, book_rate, book_author, book.getBookPrice(), book_quantity + book.getBookQuantity());
		    	  }
		    	  
		      }
		      myReader1.close();
		    } catch (FileNotFoundException e) {
		      e.printStackTrace();
		    } 
		  }  
	
}
