public class Books {
    private int bookId;
    private int categoryId;
    private String bookName;
    private double bookRate;
    private String bookAuthor;
    private double bookPrice;
    private int bookQuantity;
    private boolean bookStatus;
    
    public Books() {
    	
    }

	public Books(int bookId, int categoryId, String bookName, double bookRate, String bookAuthor, double bookPrice,
			int bookQuantity) {
		super();
		this.bookId = bookId;
		this.categoryId = categoryId;
		this.bookName = bookName;
		this.bookRate = bookRate;
		this.bookAuthor = bookAuthor;
		this.bookPrice = bookPrice;
		this.bookQuantity = bookQuantity;
		this.bookStatus = this.bookQuantity > 0;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public double getBookRate() {
		return bookRate;
	}

	public void setBookRate(double bookRate) {
		this.bookRate = bookRate;
	}

	public String getBookAuthor() {
		return bookAuthor;
	}

	public void setBookAuthor(String bookAuthor) {
		this.bookAuthor = bookAuthor;
	}

	public double getBookPrice() {
		return bookPrice;
	}

	public void setBookPrice(double bookPrice) {
		this.bookPrice = bookPrice;
	}

	public int getBookQuantity() {
		return bookQuantity;
	}

	public void setBookQuantity(int bookQuantity) {
		this.bookQuantity = bookQuantity;
	}

	public boolean isBookStatus() {
		return bookStatus;
	}

	public void setBookStatus(boolean bookStatus) {
		this.bookStatus = bookStatus;
	}
    
}
