import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CategoryDAO {
    private static JDBCConnection jdbcConnection = new JDBCConnection();

    public Categories getCategoryById(int id) {
        Categories category = new Categories();
        Connection connection = null;
        String sql = "select * from categories where category_id = ?";
        try {
            connection = jdbcConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                category.setCategoryId(resultSet.getInt("category_id"));
                category.setCategoryName(resultSet.getString("category_name"));
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return category;
    }

    public Categories getCategoryByName(String categoryName) {
        Categories category = new Categories();
        Connection connection = null;
        String sql = "select * from categories where category_name = ?";
        try {
            connection = jdbcConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, categoryName);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                category.setCategoryId(resultSet.getInt("category_id"));
                category.setCategoryName(resultSet.getString("category_name"));
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return category;
    }

    public boolean insertCategory(String categoryName) {
        Categories category = this.getCategoryByName(categoryName);
        if (category.getCategoryName() != null) return false;
        boolean check = true;
        Connection connection = null;
        String sql = "insert into categories(category_name) values(?)";
        try {
            connection = jdbcConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, categoryName);
            preparedStatement.execute();
            preparedStatement.close();
            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }

    public boolean deleteCategory(int categoryId) {
        boolean check = true;
        Connection connection = null;
        String sql = "delete from categories where category_id = ?";
        try {
            connection = jdbcConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, categoryId);
            preparedStatement.execute();
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }

    public boolean updateCategory(int categoryId, String categoryName) {
        boolean check = true;
        Connection connection = null;
        String sql = "update categories set category_name = ? where category_id = ?";
        try {
            connection = jdbcConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, categoryName);
            preparedStatement.setInt(2, categoryId);
            preparedStatement.execute();
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }

}
