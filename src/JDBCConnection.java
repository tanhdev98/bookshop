import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class JDBCConnection {
    private static String url = "jdbc:mysql://localhost:3306/bookshop";
    private static String userName = "root";
    private static String password = "123456";

    public Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(url, userName, password);
            System.out.println("connect successfully!");
        } catch (Exception ex) {
            System.out.println("connect failure!");
            ex.printStackTrace();
        }
        return connection;
    }

    public static void main(String[] args) {
        JDBCConnection jdbcConnection = new JDBCConnection();
        Connection connection = null;
        try {
            connection = jdbcConnection.getConnection();
            String sql = "select * from roles";
            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                System.out.println(resultSet.getInt("role_id") + " " + resultSet.getString("role_name"));
            }
            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
