import java.util.Date;

public class OrderDetail {
    private int orderDetailId;
    private int orderDetailPrice;
    private int orderDetailQuantity;
    private Date orderDetailDate;
    private int bookId;
    private int orderId;

    public OrderDetail() {

    }

    public OrderDetail(int orderDetailId, int orderDetailPrice, int orderDetailQuantity, Date orderDetailDate,
                       int bookId, int orderId) {
        super();
        this.orderDetailId = orderDetailId;
        this.orderDetailPrice = orderDetailPrice;
        this.orderDetailQuantity = orderDetailQuantity;
        this.orderDetailDate = orderDetailDate;
        this.bookId = bookId;
        this.orderId = orderId;
    }

    public int getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(int orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public int getOrderDetailPrice() {
        return orderDetailPrice;
    }

    public void setOrderDetailPrice(int orderDetailPrice) {
        this.orderDetailPrice = orderDetailPrice;
    }

    public int getOrderDetailQuantity() {
        return orderDetailQuantity;
    }

    public void setOrderDetailQuantity(int orderDetailQuantity) {
        this.orderDetailQuantity = orderDetailQuantity;
    }

    public Date getOrderDetailDate() {
        return orderDetailDate;
    }

    public void setOrderDetailDate(Date orderDetailDate) {
        this.orderDetailDate = orderDetailDate;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

}
