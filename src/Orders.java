public class Orders {
    private int orderId;
    private String orderAddress;
    private boolean orderStatus;
    private String orderNote;

    private int nameId;

    public Orders() {

    }

    public Orders(int orderId, String orderAddress, boolean orderStatus, String orderNote, int nameId) {
        super();
        this.orderId = orderId;
        this.orderAddress = orderAddress;
        this.orderStatus = orderStatus;
        this.orderNote = orderNote;
        this.nameId = nameId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getOrderAddress() {
        return orderAddress;
    }

    public void setOrderAddress(String orderAddress) {
        this.orderAddress = orderAddress;
    }

    public boolean isOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(boolean orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderNote() {
        return orderNote;
    }

    public void setOrderNote(String orderNote) {
        this.orderNote = orderNote;
    }

    public int getNameId() {
        return nameId;
    }

    public void setNameId(int nameId) {
        this.nameId = nameId;
    }

}
