import java.util.Date;

public class PurchaseDetail {
    private int purchaseDetailId;
    private int purchaseDetailQuantity;
    private Date purchaseDetailDate;
    private int purchaseDetailPrice;
    private int bookId;

    public PurchaseDetail() {

    }

    public PurchaseDetail(int purchaseDetailId, int purchaseDetailQuantity, Date purchaseDetailDate,
                          int purchaseDetailPrice, int bookId) {
        super();
        this.purchaseDetailId = purchaseDetailId;
        this.purchaseDetailQuantity = purchaseDetailQuantity;
        this.purchaseDetailDate = purchaseDetailDate;
        this.purchaseDetailPrice = purchaseDetailPrice;
        this.bookId = bookId;
    }

    public int getPurchaseDetailId() {
        return purchaseDetailId;
    }

    public void setPurchaseDetailId(int purchaseDetailId) {
        this.purchaseDetailId = purchaseDetailId;
    }

    public int getPurchaseDetailQuantity() {
        return purchaseDetailQuantity;
    }

    public void setPurchaseDetailQuantity(int purchaseDetailQuantity) {
        this.purchaseDetailQuantity = purchaseDetailQuantity;
    }

    public Date getPurchaseDetailDate() {
        return purchaseDetailDate;
    }

    public void setPurchaseDetailDate(Date purchaseDetailDate) {
        this.purchaseDetailDate = purchaseDetailDate;
    }

    public int getPurchaseDetailPrice() {
        return purchaseDetailPrice;
    }

    public void setPurchaseDetailPrice(int purchaseDetailPrice) {
        this.purchaseDetailPrice = purchaseDetailPrice;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

}
