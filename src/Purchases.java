public class Purchases {
    private int purchaseId;
    private String purchaseNote;
    private int supplierId;

    public Purchases() {
    }

	public Purchases(int purchaseId, String purchaseNote, int supplierId) {
		super();
		this.purchaseId = purchaseId;
		this.purchaseNote = purchaseNote;
		this.supplierId = supplierId;
	}

	public int getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(int purchaseId) {
		this.purchaseId = purchaseId;
	}

	public String getPurchaseNote() {
		return purchaseNote;
	}

	public void setPurchaseNote(String purchaseNote) {
		this.purchaseNote = purchaseNote;
	}

	public int getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(int supplierId) {
		this.supplierId = supplierId;
	}


}
