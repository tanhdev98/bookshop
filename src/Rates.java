public class Rates {
    private int rateId;
    private double rateStar;
    private String rateContent;
    private int bookId;
    private int userId;

    public Rates() {

    }

    public Rates(int rateId, double rateStar, String rateContent, int bookId, int userId) {
        super();
        this.rateId = rateId;
        this.rateStar = rateStar;
        this.rateContent = rateContent;
        this.bookId = bookId;
        this.userId = userId;
    }

    public int getRateId() {
        return rateId;
    }

    public void setRateId(int rateId) {
        this.rateId = rateId;
    }

    public double getRateStar() {
        return rateStar;
    }

    public void setRateStar(double rateStar) {
        this.rateStar = rateStar;
    }

    public String getRateContent() {
        return rateContent;
    }

    public void setRateContent(String rateContent) {
        this.rateContent = rateContent;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

}
