import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class RoleDAO {
    private static JDBCConnection jdbcConnection = new JDBCConnection();

    public Roles searchById(int id) {
        Roles role = new Roles();
        Connection connection = null;
        String sql = "select * from roles where role_id = ?";
        try {
            connection = jdbcConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                role.setRoleId(resultSet.getInt("role_id"));
                role.setRoleName(resultSet.getString("role_name"));
            }
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return role;
    }

    public Roles SearhByName(String nameRole) {
        Roles role = new Roles();
        Connection connection = null;
        String sql = "select * from roles where role_name = ?";
        try {
            connection = jdbcConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, nameRole);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                role.setRoleId(resultSet.getInt("role_id"));
                role.setRoleName(resultSet.getString("role_name"));
            }
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return role;
    }

    public boolean deleteRole(int id) {
        boolean check = true;
        Connection connection = null;
        String sql = "delete from roles where role_id = ?";
        try {
            connection = jdbcConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }

        return check;
    }

    public boolean insertRole(String roleName) {
        Roles role = this.SearhByName(roleName);
        System.out.println(role.getRoleId());
        if (role.getRoleName() != null) return false;
        boolean check = true;
        Connection connection = null;
        String sql = "insert into roles(role_name) values(?)";
        try {
            connection = jdbcConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, roleName);
            preparedStatement.execute();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }

        return check;
    }

    public boolean updateRole(int id, String newRoleName) {

        boolean check = true;
        Connection connection = null;
        String sql = "update roles set role_name = ? where role_id = ?";
        try {
            connection = jdbcConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, newRoleName);
            preparedStatement.setInt(2, id);
            preparedStatement.execute();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }

        return check;
    }

    public static void main(String[] args) {
        RoleDAO roleDAO = new RoleDAO();
//		Roles role = roleDAO.SearhByName("aaa");
        System.out.println(roleDAO.insertRole("aaaa"));


    }
}
