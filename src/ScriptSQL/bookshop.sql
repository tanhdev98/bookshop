drop database if exists bookshop;
create database bookshop;
use bookshop;

CREATE TABLE categories (
    category_id BIGINT AUTO_INCREMENT NOT NULL,
    category_name VARCHAR(255) NOT NULL,
    CONSTRAINT category_PK PRIMARY KEY (category_id)
);

CREATE TABLE suppliers (
    supplier_id BIGINT AUTO_INCREMENT NOT NULL,
    supplier_name VARCHAR(255) NOT NULL,
    supplier_address VARCHAR(255) NOT NULL,
    supplier_phone VARCHAR(20) NOT NULL,
    CONSTRAINT supplier_PK PRIMARY KEY (supplier_id)
);

CREATE TABLE books (
    book_id BIGINT AUTO_INCREMENT NOT NULL,
    category_id BIGINT NOT NULL,
    book_name VARCHAR(255) NOT NULL,
    book_rate DOUBLE NOT NULL,
    book_author VARCHAR(255) NOT NULL,
    book_price DOUBLE NOT NULL,
    book_quantity BIGINT NOT NULL,
    book_status BOOL NOT NULL,
    CONSTRAINT book_PK PRIMARY KEY (book_id),
    CONSTRAINT book_category_FK FOREIGN KEY (category_id)
        REFERENCES categories (category_id)
);

CREATE TABLE purchases (
    purchase_id BIGINT AUTO_INCREMENT NOT NULL,
    supplier_id BIGINT NOT NULL,
    purchase_note VARCHAR(255),
    CONSTRAINT purchase_PK PRIMARY KEY (purchase_id),
    CONSTRAINT purchase_supplier_FK FOREIGN KEY (supplier_id)
        REFERENCES suppliers (supplier_id)
);

CREATE TABLE purchase_detail (
    purchase_detail_id BIGINT AUTO_INCREMENT NOT NULL,
    purchase_id BIGINT NOT NULL,
    book_id BIGINT NOT NULL,
    purchase_detail_quantity BIGINT NOT NULL,
    purchase_detail_date DATE NOT NULL,
    purchase_detail_price DOUBLE NOT NULL,
    CONSTRAINT purchase_detail_PK PRIMARY KEY (purchase_detail_id),
    CONSTRAINT purchase_purchase_detail_FK FOREIGN KEY (purchase_id)
        REFERENCES purchases (purchase_id),
    CONSTRAINT purchase_book_FK FOREIGN KEY (book_id)
        REFERENCES books (book_id)
);

CREATE TABLE roles (
    role_id BIGINT AUTO_INCREMENT NOT NULL,
    role_name VARCHAR(255) NOT NULL,
    CONSTRAINT role_PK PRIMARY KEY (role_id)
);

CREATE TABLE users (
    user_id BIGINT AUTO_INCREMENT NOT NULL,
    role_id BIGINT NOT NULL,
    user_name VARCHAR(255) NOT NULL,
    user_phone VARCHAR(20) NOT NULL,
    user_address VARCHAR(255) NOT NULL,
    user_email VARCHAR(255) NOT NULL,
    CONSTRAINT user_PK PRIMARY KEY (user_id),
    CONSTRAINT user_role_FK FOREIGN KEY (role_id)
        REFERENCES roles (role_id)
);

CREATE TABLE orders (
    order_id BIGINT AUTO_INCREMENT NOT NULL,
    user_id BIGINT NOT NULL,
    order_address VARCHAR(255) NOT NULL,
    order_status BOOL NOT NULL,
    order_note VARCHAR(255),
    CONSTRAINT order_PK PRIMARY KEY (order_id),
    CONSTRAINT order_user_FK FOREIGN KEY (user_id)
        REFERENCES users (user_id)
);

CREATE TABLE order_detail (
    order_detail_id BIGINT AUTO_INCREMENT NOT NULL,
    book_id BIGINT NOT NULL,
    order_id BIGINT NOT NULL,
    order_detail_price DOUBLE NOT NULL,
    order_detail_quantity BIGINT NOT NULL,
    order_detail_date DATE NOT NULL,
    CONSTRAINT order_detail_PK PRIMARY KEY (order_detail_id),
    CONSTRAINT order_detail_book_FK FOREIGN KEY (book_id)
        REFERENCES books (book_id),
    CONSTRAINT order_detail_order_FK FOREIGN KEY (order_id)
        REFERENCES orders (order_id)
);

CREATE TABLE shipping (
    shipping_id BIGINT AUTO_INCREMENT NOT NULL,
    shipping_status BOOL NOT NULL,
    shipping_date DATE NOT NULL,
    order_id BIGINT NOT NULL,
    CONSTRAINT shiping_PK PRIMARY KEY (shipping_id),
    CONSTRAINT shipping_order_FK FOREIGN KEY (order_id)
        REFERENCES orders (order_id)
);

CREATE TABLE rates (
    rate_id BIGINT AUTO_INCREMENT NOT NULL,
    book_id BIGINT NOT NULL,
    user_id BIGINT NOT NULL,
    rate_star FLOAT NOT NULL,
    rate_content VARCHAR(255),
    CONSTRAINT rate_PK PRIMARY KEY (rate_id),
    CONSTRAINT rate_book_FK FOREIGN KEY (book_id)
        REFERENCES books (book_id),
    CONSTRAINT rate_user_FK FOREIGN KEY (user_id)
        REFERENCES users (user_id)
);
