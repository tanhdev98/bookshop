use bookshop;

insert into categories(category_name) values
("Sách Chính trị"), 
("Sách Khoa học công nghệ"), 
("Sách Văn học nghệ thuật"), 
("Sách Văn hóa xã hội"), 
("Sách Giáo trình"), 
("Sách thiếu nhi"), 
("Sách tâm linh");

insert into suppliers(supplier_name, supplier_address, supplier_phone) values 
("Nhà sách Tiền Phong", "Address 1", "093456789"),
("ADC Book", "Address 2", "093456789"),
("Trung tâm sách Đại học Sư phạm", "Address 3", "093456789"),
("Nhà sách Phương Nam", "Address 4", "093456789"),
("Nhà sách Giáo dục Hà Nội", "Address 5", "093456789"),
("Nhà sách Trí Tuệ", "Address 6", "093456789"),
("Nhà sách Fahasa", "Address 7", "093456789");

insert into books(category_id, book_name, book_rate, book_author, book_price, book_quantity, book_status) values
(1, "Sách 1", 4, "Tác giả 1", 5.5, 10, true),
(2, "Sách 2", 4.5, "Tác giả 2", 10, 10, true),
(2, "Sách 3", 5, "Tác giả 3", 5, 50, true),
(3, "Sách 4", 3, "Tác giả 4", 4, 0, false),
(3, "Sách 5", 4, "Tác giả 5", 4, 11, true),
(4, "Sách 6", 4, "Tác giả 6", 5, 12, true),
(4, "Sách 7", 4.5, "Tác giả 7", 8, 15, true),
(5, "Sách 8", 3.5, "Tác giả 8", 7, 0, false),
(6, "Sách 9", 4, "Tác giả 9", 5, 30, true),
(7, "Sách 10", 4, "Tác giả 10", 4, 12, true);

insert into purchases(supplier_id, purchase_note) values
(1, "Note 1"),
(1, "Note 1"),
(2, ""),
(2, "Note 2"),
(3, ""),
(4, "Note 4"),
(5, ""),
(6, "Note 6"),
(7, ""),
(7, "");

insert into purchase_detail(purchase_id, book_id, purchase_detail_quantity, purchase_detail_date, purchase_detail_price) values
(1, 2, 5, "2021-06-02", 2),
(2, 1, 5, "2021-05-01", 2),
(3, 3, 20, "2021-06-02", 1),
(4, 5, 5, "2021-04-02", 2),
(5, 6, 4, "2021-05-01", 2),
(6, 7, 7, "2021-06-25", 2),
(7, 9, 3, "2021-05-02", 1),
(8, 10, 10, "2021-06-02", 1);

insert into roles(role_name) values
("admin"),
("employee"),
("customer");

insert into users(role_id, user_name, user_phone, user_address, user_email) values
(1, "Admin A", "0123456789", "address 1", "root@gmail.com"),
(2, "Nhan Vien A", "0123456789", "address 2", "nva@gmail.com"),
(2, "Nhan Vien B", "0123456789", "address 3", "nvb@gmail.com"),
(2, "Nhan Vien C", "0123456789", "address 4", "nvc@gmail.com"),
(2, "Nhan Vien D", "0123456789", "address 5", "nvd@gmail.com"),
(3, "Khach Hang A", "0123456789", "address 6", "kha@gmail.com"),
(3, "Khach Hang B", "0123456789", "address 7", "khb@gmail.com"),
(3, "Khach Hang C", "0123456789", "address 8", "khc@gmail.com"),
(3, "Khach Hang D", "0123456789", "address 9", "khd@gmail.com"),
(3, "Khach Hang E", "0123456789", "address 10", "khe@gmail.com"),
(3, "Khach Hang F", "0123456789", "address 11", "khf@gmail.com"),
(3, "Khach Hang G", "0123456789", "address 12", "khg@gmail.com"),
(3, "Khach Hang H", "0123456789", "address 13", "khh@gmail.com");

insert into orders(user_id, order_address, order_status, order_note) values
(6, "address order 1", true, ""), 
(7, "address order 2", true, ""),
(8, "address order 3", true, ""),
(9, "address order 4", true, ""),
(10, "address order 5", true, ""),
(11, "address order 6", false, ""),
(12, "address order 7", true, ""),
(13, "address order 8", true, "");

insert into order_detail(book_id, order_id, order_detail_price, order_detail_quantity, order_detail_date) values
(1, 1, 5.5, 5, "2021-06-10"),
(2, 2, 10, 5, "2021-06-10"),
(2, 3, 10, 5, "2021-06-10"),
(3, 4, 5, 5, "2021-05-10"),
(3, 5, 5, 5, "2021-05-10"),
(9, 6, 5, 5, "2021-06-15"),
(10, 7, 4, 5, "2021-06-20"),
(10, 8, 4, 5, "2021-06-10");

insert into shipping(shipping_status, shipping_date, order_id) values
(true, "2021-06-10", 1),
(true, "2021-06-10", 2),
(true, "2021-06-10", 3),
(true, "2021-05-10", 4),
(true, "2021-05-10", 5),
(true, "2021-06-15", 6),
(true, "2021-06-20", 7),
(true, "2021-06-10", 8);

insert into rates(book_id, user_id, rate_star, rate_content) values
(1, 6, 4.5, "good"),
(2, 7, 5, "good"),
(3, 8, 4, "good"),
(4, 9, 4, "good"),
(5, 10, 3, "not good"),
(6, 11, 4, "good");
