use bookshop;

-- Thống kê nhập hàng từ ngày -> đến ngày
DELIMITER //
drop procedure if exists GetPurchaseDetail//
create procedure GetPurchaseDetail(in fromDate date, toDate date)
begin
	SELECT 
    *
FROM
    purchase_detail
WHERE
    purchase_detail_date BETWEEN fromDate AND toDate
ORDER BY purchase_detail_date;
end //
DELIMITER ;

call GetPurchaseDetail('2021-06-01','2021-06-31');

-- Thống kê tồn kho theo từng thể loại sách
DELIMITER //
drop procedure if exists GetQuantityBookStock//
create procedure GetQuantityBookStock()
begin
SELECT 
    b.category_id,
    category_name,
    SUM(b.book_quantity) AS book_stock
FROM
    books AS b
        INNER JOIN
    categories AS c ON b.category_id = c.category_id
GROUP BY category_id;
end //
DELIMITER ;

call GetQuantityBookStock();

-- Thống kê tồn kho theo từng sách
DELIMITER //
drop procedure if exists GetBookStock//
create procedure GetBookStock()
begin
SELECT 
    book_id ,book_name, book_quantity
FROM
    books;
end //
DELIMITER ;

call GetBookStock();

-- Thống kê doanh thu theo từ ngày -> đến ngày
DELIMITER //
drop procedure if exists GetRevenueByDate//
create procedure GetRevenueByDate(in fromDate date, toDate date)
begin
SELECT 
    order_detail_date,
    SUM(order_detail_price * order_detail_quantity) AS revenue
FROM
    order_detail
WHERE
    order_detail_date BETWEEN fromDate AND toDate
        AND order_id IN (SELECT 
            order_id
        FROM
            orders
        WHERE
            order_status = TRUE)
GROUP BY order_detail_date;
end //
DELIMITER ;

call GetRevenueByDate('2021-06-01','2021-06-30');

-- Thống kê doanh thu theo tên sách
DELIMITER //
drop procedure if exists GetRevenueByBook//
create procedure GetRevenueByBook()
begin
SELECT 
    od.book_id,
    book_name,
    SUM(order_detail_price * order_detail_quantity) AS revenue
FROM
    order_detail AS od
        INNER JOIN
    books AS b ON od.book_id = b.book_id
WHERE
    order_id IN (SELECT 
            order_id
        FROM
            orders
        WHERE
            order_status = TRUE)
GROUP BY book_id;
end //
DELIMITER ;

call GetRevenueByBook();

-- Thống kê lợi nhuận theo thời gian từ ngày -> đến ngày
DELIMITER //
drop procedure if exists GetProfit//
create procedure GetProfit(in fromDate date, toDate date)
begin
SELECT 
    od.book_id,
    purchase_detail_date,
    order_detail_date,
    SUM(order_total_price - pruchase_total_price) AS profit
FROM
    (SELECT 
        purchase_detail_id,
            book_id,
            purchase_detail_date,
            SUM(purchase_detail_quantity * purchase_detail_price) AS pruchase_total_price
    FROM
        purchase_detail
    GROUP BY book_id) AS pd
        INNER JOIN
    (SELECT 
        order_detail_id,
            book_id,
            order_detail_date,
            SUM(order_detail_price * order_detail_quantity) AS order_total_price
    FROM
        order_detail
    GROUP BY book_id) AS od ON pd.book_id = od.book_id
WHERE
    order_detail_date
        AND purchase_detail_date BETWEEN fromDate AND toDate
GROUP BY od.book_id;
end //
DELIMITER ;

call GetProfit('2021-05-01','2021-06-30');
