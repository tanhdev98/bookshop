import java.util.Date;

public class Shipping {
    private int shippingId;
    private boolean shippingStatus;
    private Date shippingDate;
    private int orderId;

    public Shipping() {

    }

    public Shipping(int shippingId, boolean shippingStatus, Date shippingDate, int orderId) {
        super();
        this.shippingId = shippingId;
        this.shippingStatus = shippingStatus;
        this.shippingDate = shippingDate;
        this.orderId = orderId;
    }

    public int getShippingId() {
        return shippingId;
    }

    public void setShippingId(int shippingId) {
        this.shippingId = shippingId;
    }

    public boolean isShippingStatus() {
        return shippingStatus;
    }

    public void setShippingStatus(boolean shippingStatus) {
        this.shippingStatus = shippingStatus;
    }

    public Date getShippingDate() {
        return shippingDate;
    }

    public void setShippingDate(Date shippingDate) {
        this.shippingDate = shippingDate;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

}
