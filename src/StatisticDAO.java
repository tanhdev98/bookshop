import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

public class StatisticDAO {
    private static JDBCConnection jdbcConnection = new JDBCConnection();
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public void GetPurchaseDetail(String fromDate, String toDate) {
        String sql = "call GetPurchaseDetail(? , ?) ";
        try {
            Connection connection = jdbcConnection.getConnection();
            CallableStatement callableStatement = connection.prepareCall(sql);
            Date date1 = dateFormat.parse(fromDate);
            Date date2 = dateFormat.parse(toDate);
            callableStatement.setDate(1, (new java.sql.Date(date1.getTime())));
            callableStatement.setDate(2, (new java.sql.Date(date2.getTime())));
            ResultSet resultSet = callableStatement.executeQuery();
            System.out.println("purchase_detail_id | purchase_id | book_id | purchase_detail_quantity | purchase_detail_date | purchase_detail_price");
            while (resultSet.next()) {
                int purchase_detail_id = resultSet.getInt("purchase_detail_id");
                int purchase_id = resultSet.getInt("purchase_id");
                int book_id = resultSet.getInt("book_id");
                int purchase_detail_quantity = resultSet.getInt("purchase_detail_quantity");
                Date purchase_detail_date = resultSet.getDate("purchase_detail_date");
                int purchase_detail_price = resultSet.getInt("purchase_detail_price");
                System.out.println(purchase_detail_id + " | " + purchase_id + " | " + book_id + " | " + purchase_detail_quantity + " | " + dateFormat.format(purchase_detail_date) + " | " + purchase_detail_price);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void GetQuantityBookStock() {
        String sql = "call GetQuantityBookStock()";
        try {
            Connection connection = jdbcConnection.getConnection();
            CallableStatement callableStatement = connection.prepareCall(sql);
            ResultSet resultSet = callableStatement.executeQuery();
            System.out.println("category_id | category_name | book_stock");
            while (resultSet.next()) {
                int category_id = resultSet.getInt("category_id");
                String category_name = resultSet.getString("category_name");
                int book_stock = resultSet.getInt("book_stock");
                System.out.println(category_id + " | " + category_name + " | " + book_stock);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void GetBookStock() {
        String sql = "call GetBookStock()";
        try {
            Connection connection = jdbcConnection.getConnection();
            CallableStatement callableStatement = connection.prepareCall(sql);
            ResultSet resultSet = callableStatement.executeQuery();
            System.out.println("book_id | book_name | book_quantity");
            while (resultSet.next()) {
                int book_id = resultSet.getInt("book_id");
                String book_name = resultSet.getString("book_name");
                int book_quantity = resultSet.getInt("book_quantity");
                System.out.println(book_id + " | " + book_name + " | " + book_quantity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void GetRevenueByDate(String fromDate, String toDate) {
        String sql = "call GetRevenueByDate(? , ?) ";
        try {
            Connection connection = jdbcConnection.getConnection();
            CallableStatement callableStatement = connection.prepareCall(sql);
            Date date1 = dateFormat.parse(fromDate);
            Date date2 = dateFormat.parse(toDate);
            callableStatement.setDate(1, (new java.sql.Date(date1.getTime())));
            callableStatement.setDate(2, (new java.sql.Date(date2.getTime())));
            ResultSet resultSet = callableStatement.executeQuery();
            System.out.println("order_detail_date | revenue");
            while (resultSet.next()) {
                Date order_detail_date = resultSet.getDate("order_detail_date");
                double revenue = resultSet.getDouble("revenue");
                System.out.println(dateFormat.format(order_detail_date) + " | " + revenue);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void GetRevenueByBook() {
        String sql = "call GetRevenueByBook()";
        try {
            Connection connection = jdbcConnection.getConnection();
            CallableStatement callableStatement = connection.prepareCall(sql);
            ResultSet resultSet = callableStatement.executeQuery();
            System.out.println("book_id | book_name | revenue");
            while (resultSet.next()) {
                int book_id = resultSet.getInt("book_id");
                String book_name = resultSet.getString("book_name");
                double revenue = resultSet.getDouble("revenue");
                System.out.println(book_id + " | " + book_name + " | " + revenue);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void GetProfit(String fromDate, String toDate) {
        String sql = "call GetProfit(? , ?) ";
        try {
            Connection connection = jdbcConnection.getConnection();
            CallableStatement callableStatement = connection.prepareCall(sql);
            Date date1 = dateFormat.parse(fromDate);
            Date date2 = dateFormat.parse(toDate);
            callableStatement.setDate(1, (new java.sql.Date(date1.getTime())));
            callableStatement.setDate(2, (new java.sql.Date(date2.getTime())));
            ResultSet resultSet = callableStatement.executeQuery();
            System.out.println("book_id | purchase_detail_date | order_detail_date | profit");
            while (resultSet.next()) {
                int book_id = resultSet.getInt("book_id");
                Date purchase_detail_date = resultSet.getDate("purchase_detail_date");
                Date order_detail_date = resultSet.getDate("order_detail_date");
                double profit = resultSet.getDouble("profit");
                System.out.println(book_id + " | " + dateFormat.format(purchase_detail_date) + " | " + dateFormat.format(order_detail_date) + " | " + profit);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        StatisticDAO statisticDAO = new StatisticDAO();
        Scanner sc = new Scanner(System.in);
        String fromDate = "", toDate = "";
        System.out.println("========Statistic========");
        System.out.println("-------------------***------------------");
        System.out.println("|   1. Statistic Purchase Detail"); // Thống kê nhập hàng từ ngày -> đến ngày
        System.out.println("|   2. Statistic Quantity Book Stock"); // Thống kê tồn kho theo từng thể loại sách
        System.out.println("|   3. Statistic Book Stock"); // Thống kê tồn kho theo từng sách
        System.out.println("|   4. Statistic Revenue By Date"); // Thống kê doanh thu theo từ ngày -> đến ngày
        System.out.println("|   5. Statistic Revenue By Book"); // Thống kê doanh thu theo sách
        System.out.println("|   6. Statistic Profit"); // Thống kê lợi nhuận theo thời gian từ ngày -> đến ngày
        System.out.println("|   0. Exit");
        System.out.println("-------------------------------------------------");
        System.out.print("Select statistic : ");
        int choice;
        do {
            choice = sc.nextInt();
            switch (choice) {
                case 1:
                    System.out.println("Input from date (yyyy-MM-dd)");
                    fromDate = new Scanner(System.in).nextLine();
                    System.out.println("Input to date (yyyy-MM-dd)");
                    toDate = new Scanner(System.in).nextLine();
                    statisticDAO.GetPurchaseDetail(fromDate, toDate);
                    break;
                case 2:
                    statisticDAO.GetQuantityBookStock();
                    break;
                case 3:
                    statisticDAO.GetBookStock();
                    break;
                case 4:
                    System.out.println("Input from date (yyyy-MM-dd)");
                    fromDate = new Scanner(System.in).nextLine();
                    System.out.println("Input to date (yyyy-MM-dd)");
                    toDate = new Scanner(System.in).nextLine();
                    statisticDAO.GetRevenueByDate(fromDate, toDate);
                    break;
                case 5:
                    statisticDAO.GetRevenueByBook();
                    break;
                case 6:
                    System.out.println("Input from date (yyyy-MM-dd)");
                    fromDate = new Scanner(System.in).nextLine();
                    System.out.println("Input to date (yyyy-MM-dd)");
                    toDate = new Scanner(System.in).nextLine();
                    statisticDAO.GetProfit(fromDate, toDate);
                    break;
            }
        } while (!(choice == 0));
    }
}

