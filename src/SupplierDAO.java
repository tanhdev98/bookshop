import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SupplierDAO {
    private static JDBCConnection jdbcConnection = new JDBCConnection();

    public Suppliers getSupplierById(int supplierId) {
        Suppliers suppliers = new Suppliers();
        Connection connection = null;
        String sql = "select * from suppliers where supplier_id = ?";
        try {
            connection = jdbcConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, supplierId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                suppliers.setSupplierID(resultSet.getInt("supplier_id"));
                suppliers.setSupplierName(resultSet.getString("supplier_name"));
                suppliers.setSupplierAddress(resultSet.getString("supplier_address"));
                suppliers.setSupplierPhone(resultSet.getString("supplier_phone"));

            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return suppliers;
    }

    public Suppliers getSupplierByName(String supplierName) {
        Suppliers suppliers = new Suppliers();
        Connection connection = null;
        String sql = "select * from suppliers where suppiler_name = ?";
        try {
            connection = jdbcConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, supplierName);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                suppliers.setSupplierID(resultSet.getInt("supplier_id"));
                suppliers.setSupplierName(resultSet.getString("supplier_name"));
                suppliers.setSupplierAddress(resultSet.getString("supplier_address"));
                suppliers.setSupplierPhone(resultSet.getString("supplier_phone"));

            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return suppliers;
    }

    boolean insertSuppiler(String supplier_name, String supplier_address, String supplier_phone) {
        Suppliers suppliers = this.getSupplierByName(supplier_name);
        if (suppliers.getSupplierName() != null) return false;
        boolean check = true;
        Connection connection = null;
        String sql = "insert into suppliers(supplier_name, supplier_address, supplier_phone) values (?, ?, ?)";

        try {
            connection = jdbcConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, supplier_name);
            preparedStatement.setString(2, supplier_address);
            preparedStatement.setString(3, supplier_phone);
            preparedStatement.execute();
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }

    boolean deleteSupplier(int supplierId) {
        boolean check = true;
        Connection connection = null;
        String sql = "delete from suppliers where supplier_id = ?";
        try {
            connection = jdbcConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, supplierId);
            preparedStatement.execute();
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            check = false;
            e.printStackTrace();
            // TODO: handle exception
        }
        return check;
    }

    boolean updateSupplier(int supplier_id, String supplier_name, String supplier_address, String supplier_phone) {
        boolean check = true;
        Connection connection = null;
        String sql = "update suppliers set suppiler_name = ?, supplier_address = ?, supplier_phone = ? where supplier_id = ?";
        try {
            connection = jdbcConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, supplier_name);
            preparedStatement.setString(2, supplier_address);
            preparedStatement.setString(3, supplier_phone);
            preparedStatement.setInt(4, supplier_id);
            preparedStatement.execute();
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
            // TODO: handle exception
        }
        return check;
    }


}
